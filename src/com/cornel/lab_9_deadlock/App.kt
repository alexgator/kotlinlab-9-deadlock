package com.cornel.lab_9_deadlock


fun main(args: Array<String>) {
    val lock1 = Any()
    val lock2 = Any()
    val t1 = object : Thread() {
        override fun run() {
            synchronized(lock1) {
                println("Thread #1 get lock #1")
                try {
                    Thread.sleep(5)
                } catch (e: Throwable) {}

                synchronized(lock2) {
                    println("Thread #1 get lock #2")
                }

            }
        }
    }
    val t2 = object : Thread() {
        override fun run() {
            synchronized(lock2) {
                println("Thread #2 get lock #2")
                try {
                    Thread.sleep(5)
                } catch (e: Throwable) {}

                synchronized(lock1) {
                    println("Thread #2 get lock #1")
                }

            }
        }
    }
    t1.start()
    t2.start()
}